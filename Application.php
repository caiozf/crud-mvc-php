<?php


	class Application
	{
		
		public function executar(){
			$url = isset($_GET['url']) ? explode('/', $_GET['url'])[0] : 'Home';
			$url = ucfirst($url);

			$url.="Controller";

			if(file_exists("Controllers/".$url.'.php')){
				$className = "Controllers\\".$url;
				$controller = new $className;
				$controller->executar(); 
			}else{
				echo "ops! Este Controller não existe";
				echo "<br />";
				echo "Tente acessar a rota '/contato' ";
				die();
			}
		}
	}

?>