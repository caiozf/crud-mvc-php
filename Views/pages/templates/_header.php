<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php echo self::titulo; ?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="collapse navbar-collapse">
				<a class="nav-link" href="/crud-mvc/lista">Listagem</a>
				<a class="nav-link" href="/crud-mvc/contato">Novo contato</a>
			</div>
		</nav>
	</header>
